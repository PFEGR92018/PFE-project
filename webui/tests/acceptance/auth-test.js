import { test } from 'qunit';
import moduleForAcceptance from 'webui/tests/helpers/module-for-acceptance';
import { selectorToExist } from 'ember-wait-for-test-helper/wait-for';

moduleForAcceptance('Acceptance | auth');

test('visiting_auth', function(assert) {
  visit('');

  andThen(function() {
    assert.equal(currentURL(), '');
  });
});


test('connect_auth', function(assert) {
  visit('');
  fillIn('#InputEmail', 'admin@vinci.be');
  fillIn('#InputPassword', 'bb');
  click('#login_btn');
  waitFor(currentURL()==="/admin/dashboard");
 
  andThen(() => {
    assert.ok(find('#title_admin').length === 1);
  });
    
  

});






