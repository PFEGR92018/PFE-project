import { test } from 'qunit';
import moduleForAcceptance from 'webui/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | machines qr');

test('visiting /admin/machines', function(assert) {
  visit('/admin/machines');

  andThen(function() {
    assert.equal(currentURL(), '/admin/machines');
  });
});

test('modal', function(assert) {
  visit('/admin/machines');
  click('#print_qr');
  //andThen(() =>click('#print_btn'));
  andThen(() => assert.equal(find('#print_btn').name(), 'My new post'));
});

