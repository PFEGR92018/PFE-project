import Controller from "@ember/controller";
import $ from "jquery";

const { Logger } = Ember;

export default Controller.extend({
  reports: null,


  actions: {
    setPriority(report) {
      //alert(report);
      report.priority = $('#select_level').val();
      //let formData = {};
      //formData.priority = this.get("priority");
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://localhost:4500/report/changePriority",
        data: JSON.stringify(report),
        success: function (result, status, xhr) {
          alert("La priorité a été changé avec succès.");
        },
        error: function (xhr, status, error) {
          alert("Problème lors du changement de priorité.");
        },
        complete: function (xhr, status) {
          // alert("complete");
        }
      });
    },
    setState(report) {
      report.state = $('#select_state').val();

      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://localhost:4500/report/changeState",
        data: JSON.stringify(report),
        success: function (result, status, xhr) {
          alert("L'état a été changé avec succès.");
        },
        error: function (xhr, status, error) {
          alert("Problème lors du changement d'état.");
        },
        complete: function (xhr, status) {
        }
      });
    }
  }
});
