import Controller from "@ember/controller";
import json from "ember-data/serializers/json";
const { Logger } = Ember;
var html_to_insert="";
export default Controller.extend({
  machines: null,

  actions: {
    upload(event) {
      var input = event.target;

      for (var i = 0; i < input.files.length; i++) {
        readF(input.files[i]);
      }
    },
    generateQrCodes() {
      var crtl = this;
      findAllActiveMachines(function(err, result) {
        if (err) {
        } else {
          crtl.set("machines", result);
          //Logger.log(crtl.get("machines"));
        }
      });
    },
    downloadPdf() {
      var canvas = document.getElementsByClassName("qr-code");
      Logger.log("ici: " + canvas.length);
      var pdf = new jsPDF("p", "pt", "letter");
      pdf.setFontSize(10);
      var height = 0;
      var margin = 20;
      var cptQrCodes = 0;
      for (var i = 0; i < canvas.length; i++) {
        var current = canvas[i];
        var imgData = current.toDataURL("image/jpeg", 1.0);
        pdf.text(
          current.parentElement.getAttribute("data-info") +
            " : scannez pour rapporter un problème.",
          margin,
          height + 30
        );
        pdf.addImage(imgData, "JPEG", margin, height + 50);
        margin += 300;
        if (margin > 600) {
          margin = 20;
          height += 200;
        }
        cptQrCodes++;
        if (cptQrCodes == 8) {
          pdf.addPage();
          cptQrCodes = 0;
          height=0;
          margin=20;
        }
        Logger.log(height);
      }
      pdf.save("QRCodes_Machines.pdf");
    }
  }
});

function readF(file) {
  var reader = new FileReader();
  var local = file.name.slice(-7, -4);

  console.log(local);

  reader.onload = function() {
    var text = reader.result;
    var test = text.split("\r\n");
    test.shift();
    /*
    var test2 = test[0].split(";");
    console.log(test2);
    */
    var myArray = [];
    for (var i = 0; i < test.length - 1; i++) {
      //console.log(test[i]);
      var element = test[i].split(";");
      //console.log(element);

      var x = element[3].replace(/"/g, "");
      //console.log("Tout beau : "+x);

      var retJson = {
        name: "",
        macAddress: "",
        comment: "",
        active: true,
        ip: "",
        local: ""
      };
      retJson.name = element[1].replace(/"/g, "");
      retJson.macAddress = element[2].replace(/"/g, "");
      retJson.comment = element[3].replace(/"/g, "");
      retJson.ip = element[0].replace(/"/g, "");
      retJson.active = true;
      retJson.local = local;
      myArray.push(retJson);
      html_to_insert+="<tr>"+
      "<td data-title=\"Nom\">"+element[1].replace(/"/g, "")+"</td>"+
      "<td data-title=\"IP\">"+element[0].replace(/"/g, "")+"</td>"+
     "<td data-title=\"MAC address\">"+element[2].replace(/"/g, "")+"</td>"+
      "<td data-title=\"Commentaire\">"+element[3].replace(/"/g, "")+"</td>"+
      "<td data-title=\"Actif\">true</td>"+
      "<td data-title=\"Local\">"+local+"</td>"+
    "</tr>" ;

      //console.log("retJson : "+retJson.comment);
    }

    var myJson = JSON.stringify(myArray);
    submitMachinesList(myJson);
  };
  console.log("ReadF2" + file);
  reader.readAsText(file);
}

function submitMachinesList(json) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: "http://localhost:4500/admin/machines",
    // dataType: 'JSON',
    // method: 'POST',
    data: json,
    success: function(result, status, xhr) {
      alert("success " + result.message);
      refreshTable();
      
    },
    error: function(xhr, status, error) {
      alert("error" + error);
      html_to_insert="";
    },
    complete: function(xhr, status) {
      alert("complete");
    }
  });
}

function findAllActiveMachines(callback) {
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: "http://localhost:4500/admin/machines",
    success: function(result, status, xhr) {
      Logger.log(result);
      return callback(null, result.machine);
      // alert("success" + JSON.stringify(result));
    },
    error: function(xhr, status, error) {
      // alert("error");
    },
    complete: function(xhr, status) {
      // alert("complete");
    }
  });
}

function refreshTable(){
  $('#table_machines').append( html_to_insert);
  html_to_insert="";
}
