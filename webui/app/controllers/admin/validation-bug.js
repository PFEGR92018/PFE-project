import Controller from '@ember/controller';
import { empty, and, match, not } from "@ember/object/computed";
import EmberObject, {
    computed,
    observer
  } from '@ember/object';

export default Controller.extend({
    actions: {
        validate(item){
            console.log(item);
            item.data.state = "validate";
            console.log(item);
            sendServ(item);
        },
        denie(item){
            console.log(item);
            rmServ(item);
        }
    }
});

function rmServ(item) {
    var myJson = JSON.stringify(item);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://localhost:4500/report/removeReport",
        // dataType: 'JSON',
        // method: 'POST',
        data: myJson,
        success: function(result, status, xhr) {
          alert("Le rapport a été rejeté avec succès.");

        },
        error: function(xhr, status, error) {
          alert("Problème lors du rejet du rapport.");
        },
        complete: function(xhr, status) {
          // alert("complete");
        }
      });
}

function sendServ(item) {
    var myJson = JSON.stringify(item);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://localhost:4500/report/changeState",
        // dataType: 'JSON',
        // method: 'POST',
        data: myJson,
        success: function(result, status, xhr) {
          alert("Le rapport a été accepté avec succès.");

        },
        error: function(xhr, status, error) {
          alert("Problème lors de la validation du rapport.");
        },
        complete: function(xhr, status) {
          // alert("complete");
        }
      });
}
