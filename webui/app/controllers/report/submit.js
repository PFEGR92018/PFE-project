import Controller from "@ember/controller";
import $ from "jquery";
import { empty, and, match, not } from "@ember/object/computed";
import EmberObject, {
  computed,
  observer
} from '@ember/object';
const { Logger } = Ember;

export default Controller.extend({
  isDisabled: false,
  machineID: null,
  emailQuidam: "",
  emailErrorMessage: "",
  descriptionMessage: "",
  descriptionErrorMessage: "",
  priority: null,
  pictureURL: '',
  pictureURLObserver: observer('pictureURL',function(){
    console.log("ici: " + this.get('pictureURL'))
  }),
  pictureURLComputed: computed('pictureURL', function(){
    return this.get("pictureURL")
  }),
  actions: {
    submitReport() {
      var pattern = /^.+@(vinci.be|student.vinci.be)$/;
      if (!pattern.test(this.get("emailQuidam")) || this.get("descriptionMessage") == "") {
        if (!pattern.test(this.get("emailQuidam"))) {
          $('#emailErrorMessage').text("email invalide!");
        } else {
          $('#emailErrorMessage').text("");
        }
        if (this.get("descriptionMessage") == "") {
          $('#descriptionErrorMessage').text("la description ne doit pas être vide!");
        } else {
          $('#descriptionErrorMessage').text("");
        }
        return;
      }
      let formData = {};
      formData.emailQuidam = this.get("emailQuidam");
      formData.machineName = this.get("machine.id");
      formData.description = this.get("descriptionMessage");
      formData.priority = this.get("priority");
      formData.picture = this.get("pictureURL");

      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://localhost:4500/report/submit",
        data: JSON.stringify(formData),
        success: function (result, status, xhr) {
          alert("Votre rapport a été soumis avec succès. Veuillez le valider en cliquant sur le mail qu'il vous a été envoyé.");
        },
        error: function (xhr, status, error) {
          alert("Problème lors de la soumission de votre rapport.Réessayez.");
        },
        complete: function (xhr, status) {
          // alert("complete");
        }
      });
    },
    setPriority(selected) {
      this.set("priority", selected);
    },
    uploadPicture: function (files) {
      var ctrl = this;
      Logger.log("dans uploadfile");
      var files = $("#picture-input").get(0).files;
      var formData = new FormData();
      var file = files[0];
      formData.append("picture", file, file.name);
      $("#pogress-bar").css('display', 'block');
      $.ajax({
        url: "http://localhost:4500/report/submit/pictures",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        xhr: function () {
          var xhr = new XMLHttpRequest();

          // Add progress event listener to the upload.
          xhr.upload.addEventListener("progress", function (event) {
            var progressBar = $(".progress-bar");

            if (event.lengthComputable) {
              var percent = event.loaded / event.total * 100;
              progressBar.width(percent + "%");

              if (percent === 100) {
                progressBar.removeClass("active");
              }
            }
          });

          return xhr;
        },
        success: function (result, status, xhr) {
          ctrl.set("pictureURL", result.filename);
        },
        error: function (xhr, status, error) {
          alert("Problème lors de la soumission de votre photo.Réessayez.");
        },
        complete: function (xhr, status) {
          // alert("complete");
        }
      });
    },
  }
});
