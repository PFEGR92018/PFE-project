import Controller from "@ember/controller";
import { inject } from "@ember/service";

// import $ from "jquery";

export default Controller.extend({
  session: inject(),
  isDisabled: false,

  username: "",
  password: "",

  actions: {
    authenticate: function() {
      var credentials = this.getProperties("username", "password"),
        authenticator = "authenticator:jwt";
      this.get("session")
        .authenticate(authenticator, credentials)
        .catch(reason => {
          this.set("errorMessage", reason.error || reason);
        });
    }
  }
  // let formData = {};
  // formData.username = this.get("username");
  // formData.password = this.get("password");

  // $.ajax({
  //   type: "POST",
  //   contentType: "application/json",
  //   url: "http://localhost:4500/auth/signin",
  //   data: JSON.stringify(formData),
  //   success: function(result, status, xhr) {
  //     alert("success" + JSON.stringify(result));
  //   },
  //   error: function(xhr, status, error) {
  //     alert("error");
  //   },
  //   complete: function(xhr, status) {
  //     alert("complete");
  //   }
  // });
});
