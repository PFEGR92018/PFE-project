import DS from "ember-data";

export default DS.Model.extend({
  _id: DS.attr("string"),
  emailQuidam: DS.attr("string"),
  machineName: DS.attr("string"),
  description: DS.attr("string"),
  picture: DS.attr("string"),
  state: DS.attr("string", {
    options: ["submit", "validate", "in_process", "resolved", "unresolved"],
    defaultValue: "submit"
  }),
  date: DS.attr("string"),
  priority: DS.attr("string", {
    options: ["0", "1", "2", "3", "4", "5"],
  }),
  isVerified: DS.attr("boolean", {
    defaultValue() {
      return true;
    }
  })
});
