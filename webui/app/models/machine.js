import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    macAddress: DS.attr('string'),
    comment: DS.attr('string'),
    active: DS.attr('string'),
    ip: DS.attr('string'),
    local: DS.attr('string'),
});

/*
name: { type: String, required: true, unique: true },
  macAddress: { type: String, required: true },
  comment: { type: String },
  active: { type: Boolean, default: true }
*/
