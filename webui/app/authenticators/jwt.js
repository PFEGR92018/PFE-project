import Ember from "ember";
import Base from "ember-simple-auth/authenticators/base";
import config from "../config/environment";

const { RSVP: { Promise }, $: { ajax }, run } = Ember;
const { Logger } = Ember;

export default Base.extend({
  tokenEndpoint: `${config.host}/auth/signin`,
  restore(data) {
    return new Promise((resolve, reject) => {
      if (!Ember.isEmpty(data.token)) {
        resolve(data);
      } else {
        reject();
      }
    });
  },
  authenticate(creds) {
    const { username, password } = creds;
    const data = JSON.stringify({
      username: username,
      password: password
    });
    const requestOptions = {
      url: this.tokenEndpoint,
      type: "POST",
      data: data,
      contentType: "application/json",
      dataType: "json"
    };
    return new Promise((resolve, reject) => {
      ajax(requestOptions).then(
        response => {
          // Logger.log(response);
          const { jwt } = response.id_token;
          // Wrapping aync operation in Ember.run
          run(() => {
            resolve({
              token: jwt
            });
          });
        },
        error => {
          // Wrapping aync operation in Ember.run
          run(() => {
            reject(error);
          });
        }
      );
    });
  },
  invalidate(data) {
    return Promise.resolve(data);
  }
});
