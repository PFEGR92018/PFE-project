import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    namespace: '/report',
    host: 'http://localhost:4500',
    urlForFindAll(modelName, snapshot) {
        return this.host+this.namespace+'/getSubmitReports';
    }
});
