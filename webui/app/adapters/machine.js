import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    namespace: '/admin',
    host: 'http://localhost:4500',
    urlForFindAll() {
        return this.host+this.namespace+'/machines';
    }
    
});
