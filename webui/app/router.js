import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('admin', function() {
    this.route('machines', { path: '/machines'});
    this.route('dashboard', { path: '/dashboard'});
    this.route('validation-bug', { path: '/validation-bug'});
    this.route('test', { path: '/test'});
  }),
  this.route('report', function() {
    this.route('submit', { path: '/submit/:machine_id' });
    this.route('verify', { path: '/verify/:token' });
  })
  this.route('auth', { path: '/'});
});

export default Router;
