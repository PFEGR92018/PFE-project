//import Ember from 'ember';
import Route from '@ember/routing/route';

export default Route.extend({
  model(params){
    // alert("Param in Route: " + params.machine_id);
    return {id: params.machine_id};
  },
  setupController(controller, model) {
    this._super(...arguments); // this calls the default behavior of setting your model in the controller to the return value from your model hook
    controller.set('machine', model); // This sets the property 'chat' in your controller to your model as well
  }
});
