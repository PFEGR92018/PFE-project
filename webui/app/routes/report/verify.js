import Route from '@ember/routing/route';
import $ from "jquery";

export default Route.extend({model(params){
  // alert("Param in Route: " + params.machine_id);
  return {id: params.token};
},
setupController(controller, model) {
  this._super(...arguments); // this calls the default behavior of setting your model in the controller to the return value from your model hook
  controller.set('token', model); // This sets the property 'chat' in your controller to your model as well
  $.ajax({
    type: 'GET',
    contentType: "application/json",
    url: "http://localhost:4500/report/verify/" + model.id,
    success: function(result, status, xhr) {
      alert("Votre rapport a été validé avec succès.");
    },
    error: function(xhr, status, error) {
      alert("Problème lors de la validation de votre rapport.");
    },
    complete: function(xhr, status) {
      // alert("complete");
    }
  });

}
});
