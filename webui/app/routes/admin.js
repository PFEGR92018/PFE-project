import Route from "@ember/routing/route";
import { inject } from "@ember/service";
// import RSVP from 'rsvp';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  session: inject(),

});
// export default Route.extend({

// });
