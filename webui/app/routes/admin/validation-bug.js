import Route from '@ember/routing/route';
import { inject } from "@ember/service";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  session: inject(),

  model() {
    return this.store.findAll("bugstovalidate");

    /*
    return [{
        email:"mail",
        description:"desc",
        photo:"photo",
        name:"name",
        date:"0000",
        priorite:"0"
    }];
    */
  }
});
