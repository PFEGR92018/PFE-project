import Route from "@ember/routing/route";
import { inject } from "@ember/service";
// import RSVP from 'rsvp';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  session: inject(),

  model() {
    //var x = this.store.findAll("report");

    return this.store.findAll("report");
  }
});


/* import Route from '@ember/routing/route';
import { inject } from "@ember/service";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import $ from "jquery";
const { Logger } = Ember;

export default Route.extend(AuthenticatedRouteMixin, {
  session: inject(),
  // model(){
  //   return $.getJSON('http://localhost:4500/report/validate').then((data) => {
  //     this.get('store').pushPayload('report', data.obj);
  //     return this.get('store').peekAll('report');
  //   });
  // }
  setupController(controller, model) {
    this._super(...arguments);
    loadReports(controller);
  }
});

function loadReports(controller){
  $.ajax({
    type: 'GET',
    contentType: "application/json",
    url: "http://localhost:4500/report/validate/",
    success: function(result, status, xhr) {
      Logger.log(result);
      controller.set("reports", result.obj);
      alert("success");
    },
    error: function(xhr, status, error) {
      alert("error");
    },
    complete: function(xhr, status) {
      // alert("complete");
    }
  });
}

function loadUserReports(controller){

}
*/