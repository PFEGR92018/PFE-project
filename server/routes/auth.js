var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
var crypto = require("crypto");

var User = require("../models/user");

var isAuthenticated = function(req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated()) return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect("/");
};

function generateToken(user) {
  var token = jwt.sign(
    { user: user },
    process.env.JWT_SECRET || "ThisIsSoSecretBro",
    { expiresIn: 7 * 24 * 60 * 60 * 1000 /*1 week */ }
  );
  return token;
}

module.exports = function(passport) {
  router.post("/signin", function(req, res, next) {
    passport.authenticate("login", function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({
          message: "Incorrect email or password."
        });
      }
      req.logIn(user, function(err) {
        if (err) {
          return res.status(500).json({
            message: "Could not log in user. Try again later."
          });
        }
        user.password = "";

        res.status(200).json({
          message: "Connexion réussie",
          // obj: user,
          id_token: generateToken(user)
        });
      });
    })(req, res, next);
  });

  router.get("/logout", function(req, res) {
    req.logout();
    req.session.destroy();
    return res.status(200).json({
      message: "successful disconnection."
    });
  });

  return router;
};
