import DS from 'ember-data';

export default DS.Model.extend({
    emailQuidam: DS.attr('string'),
    machineName: DS.attr('string'),
    description: DS.attr('string'),
    picture: DS.attr('string'),
    date: DS.attr('string'),
    priority: DS.attr('string'),
    _id: DS.attr('string'),
    state: DS.attr('string'),
});
