var express = require("express");
var router = express.Router();
/*
var Machine = require('../models/machine');
var mongoose = require("mongoose");
*/
var mongoose = require("mongoose");
var Machine = mongoose.model("machine");

//const Transaction = require('mongoose-transactions');

router.get("/machines", function(req, res) {
  Machine.find({ active: true }, function(err, docs) {
    if (err) {
      return res.send({ error: err });
    } else {
      return res.status(200).json({
        machine: docs
      });
    }
  });
});

router.post("/machines", function(req, res) {
  console.log(req.body);

  var numLocal = req.body[0].local;

  /* Pass all atributes of machine of the numLocal to false */
  var conditions = { local: numLocal };
  var update = { $set: { active: false } };
  var options = { multi: true, new: true };

  Machine.update(conditions, update, options, callback);

  function callback(err, numAffected) {
    // numAffected is the number of updated documents
    console.log(numAffected);

    /* Run through all the machine and Insert or Update each machine to the Db*/
    req.body.forEach(element => {
      var machine = {
        name: element.name,
        macAddress: element.macAddress,
        comment: element.comment,
        active: element.active,
        ip: element.ip,
        local: element.local
      };
      var conditions = { name: machine.name };
      var update = machine;
      var options = { upsert: true };
      Machine.findOneAndUpdate(conditions, update, options, function(err, doc) {
        if (err) {
          return res.status(500).json({
            title: "An error occured",
            error: err
          });
        }
      });
    });

    return res.status(201).json({
      message: "Your machines had been saved."
    });
  }

  /* Run through all the machine and Insert or Update each machine to the Db*/

  /*
	Test transaction mais problème avec le model donnée au update
		var x = "machine";
		const transaction = new Transaction();
		// create operation on transaction instance
		machines.forEach(element =>{
			//element.active = false;
			//transaction.update('machine', element._id, element);
			transaction.update(x, element._id, {active: false});
		});
		
		// get and save created operation, saveOperations method  return the transaction id saved on database
		const operations = transaction.getOperations();
		const transId = transaction.saveOperations();
	 
		// create a new transaction instance
		const newTransaction = new Transaction(true);
	 
		// load the saved operations in the new transaction instance using the transId
		newTransaction.loadDbTransaction(transId);
	 
		// if you need you can get the operations object 
		const newOperations = newTransaction.getOperations();
	 
		// finally run and rollback
		try {
			const final = newTransaction.run()
		} catch (err) {
			const rolled = newTransaction.rollback()
			console.log(err);
		}

	*/
});

module.exports = router;
