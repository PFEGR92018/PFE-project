var express = require("express");
var router = express.Router();
var path = require("path");
var fs = require("fs");
var formidable = require("formidable");
var readChunk = require("read-chunk");
var fileType = require("file-type");

var report_controller = require("../controllers/report");

var mongoose = require('mongoose');
var Report = mongoose.model('report');

var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated()) return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect("/");
};


router.post("/submit", function (req, res) {
  report_controller.createReport(req.body, function (err, done) {
    if (err) {
      return res.status(500).json({});
    }
    res.status(200).json({});
  });
});

router.post("/submit/pictures", function (req, res) {
  var picture,
    form = new formidable.IncomingForm();
  // Upload directory for the images
  form.uploadDir = path.join(__dirname, "tmp_uploads");

  // Invoked when a file has finished uploading.
  form.on("file", function (name, file) {

    var buffer = null,
      type = null,
      filename = "";

    // Read a chunk of the file.
    buffer = readChunk.sync(file.path, 0, 262);
    // Get the file type using the buffer read using read-chunk
    type = fileType(buffer);

    // Check the file type, must be either png,jpg or jpeg
    if (
      type !== null &&
      (type.ext === "png" || type.ext === "jpg" || type.ext === "jpeg")
    ) {
      // Assign new file name
      filename = Date.now() + "-" + file.name;

      // Move the file with the new file name
      fs.rename(file.path, path.join(__dirname, "/../../webui/public/assets/images/" + filename));

      // Add to the list of photos
      picture = {
        status: true,
        filename: filename,
        type: type.ext,
        publicPath: "uploads/" + filename
      };
    } else {
      picture = {
        status: false,
        filename: file.name,
        message: "Invalid file type"
      };
      fs.unlink(file.path);
    }
  });

  form.on("error", function (err) {
    console.log("problème lors de l'upload de la photo - " + err);
  });

  // Invoked when all the fields have been processed.
  form.on("end", function () {
    console.log("Photo sauvée");
  });

  // Parse the incoming form fields.
  form.parse(req, function (err, fields, files) {
    res.status(200).json(picture);
  });
});

router.get("/verify/:token", function (req, res, next) {
  console.log(req.params.token);
  report_controller.verifyReport(req.params, function (err, done) {
    if (err) {
      console.log("erreur ici");
      return res.status(500).json({

      })
    }
    return res.status(200).json({
      message: "Rapport bien validé.",
      obj: done
    })
  });
});

router.get("/getValidateReports", function (req, res, next) {
  report_controller.validateReports(req, function (err, done) {
    if (err) {
      console.log("erreur validate");
      return res.status(500).json({
        message: "Problème /report/validate"
      })
    }
    console.log("ok validate");
    console.log(done);
    return res.status(200).json({
      report: done
    })
  });
});

router.get("/getSubmitReports", function (req, res, next) {
  report_controller.submitReports(req, function (err, done) {
    if (err) {
      console.log("erreur submit");
      return res.status(500).json({
        message: "Problème /report/submit"
      })
    }
    return res.status(200).json({
      bugstovalidate: done
    })
  });
});

//Receive the report with the new priority
router.post("/changePriority", function (req, res, next) {
  var report = req.body;
  console.log(report);
  report_controller.changeReport(report, function (err) {
    if (err) {
      console.log("erreur changePriority");
      return res.status(500).json({
        message: "Problème /report/changePriority"
      })
    }
    return res.status(200).json({
      message: "La priorité a bien été changée."
    })
  });
});

router.post("/changeState", function (req, res, next) {
  var report = req.body;
  report_controller.changeReport(report, function (err) {
    if (err) {
      console.log("erreur changeState");
      return res.status(500).json({
        message: "Problème /report/changeState"
      })
    }
    return res.status(200).json({
      message: "Le state a bien été changée."
    })
  });
});



//remove the report
router.post("/removeReport", function (req, res, next) {
  var report = req.body;
  //console.log("---------------------------------------------------------------")
  //console.log(req);
  report_controller.removeReport(report, function (err) {
    if (err) {
      console.log("erreur changeState");
      return res.status(500).json({
        message: "Problème /report/changeState"
      })
    }
    return res.status(200).json({
      message: "Le state a bien été changée."
    })
  });
});

module.exports = router;
