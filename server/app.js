var express = require("express");
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var expressSession = require('express-session');


var mongoose = require("mongoose");
var Machine = require('./models/machine');
var User = require('./models/user');
var Report = require('./models/report');

var app = express();

var dbconnexion = "mongodb://localhost:27017/pfe";
//mongoose.Promise = require('bluebird');
mongoose.connect(dbconnexion);

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser(process.env.JWT_SECRET || 'ThisIsSoSecretBro'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressSession({
  saveUninitialized: true,
  resave: false,
  // store: sessionStorage,
  secret: process.env.JWT_SECRET || 'ThisIsSoSecretBro',
  // cookie: {
      // path: '/',
      // httpOnly: true,
      // secure: false,
      // maxAge: 7 * 24 * 60 * 60 * 1000 // 1 wekk
  // }
}));
app.use(passport.initialize());
app.use(passport.session());


// A check
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

var initPassport = require('./passport/init');
initPassport(passport);

var authRoutes = require("./routes/auth")(passport);
app.use("/auth", authRoutes);
var reportRoutes = require("./routes/report");
app.use("/report", reportRoutes);

var adminRoutes = require("./routes/admin");
app.use("/admin", adminRoutes);


var bCrypt = require('bcryptjs');
var usr = new User({ email: "admin@vinci.be", password: bCrypt.hashSync("bb",10)});

// var usr = new User({ email: "hello@hello.com", password: "coucou"});

var machine = new Machine({name: 'oui',
  macAddress: 'oui',
  comment: 'oui',
  active: true})

/*var report = new Report({
  emailQuidam: "mail@vinci.be",
  machineName: "LEN1421",
  description: "aaaa",
  state: "submit",
  date: Date.now,
  isVerified: true

});

report.save(function (err, result) {

});
*/

usr.save(function (err, result) {

});

module.exports = app;
