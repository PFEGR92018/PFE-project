var mongoose = require("mongoose");
var Schema = mongoose.Schema;
// mongoose unique validator

var userSchema = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true } // A crypter
});

module.exports = mongoose.model("user", userSchema);
