var mongoose = require("mongoose");
var Schema = mongoose.Schema;
// mongoose unique validator

var reportSchema = new Schema({
  emailQuidam: { type: String, required: true },
  machineName: { type: String, required: true },
  description: { type: String, required: true },
  picture: { type: String },
  state: {
    type: String,
    enum: ["submit", "validate", "in_process", "resolved", "unresolved"],
    default: "submit",
    required: true
  },
  date: { type: Date, required: true, default: Date.now },
  priority: {
    type: Number,
    enum: [1, 2, 3, 4, 5],
    default: 0
  },
  isVerified: {
    type: Boolean,
    default: false // A changer après l'envoie d'email
  },
  responsible: [
    {
      type: Schema.Types.ObjectId,
      ref: "user"
    }
  ]
});

module.exports = mongoose.model("report", reportSchema);
