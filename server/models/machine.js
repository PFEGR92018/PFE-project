var mongoose = require("mongoose");
var Schema = mongoose.Schema;
// mongoose unique validator

var machineSchema = new Schema({
  name: { type: String, required: true, unique: true },
  macAddress: { type: String, required: true },
  comment: { type: String },
  active: { type: Boolean, default: true },
  ip: { type: String},
  local: { type: String}
});

module.exports = mongoose.model("machine", machineSchema);
