var mongoose = require("mongoose");
var Schema = mongoose.Schema;

const tokenSchema = new mongoose.Schema({
    _reportId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "Report" },
    token: { type: String, required: true, index: { unique: true } },
    date_created: {
      type: Date,
      required: true,
      default: Date.now,
      expires: 43200
    }
  });

 module.exports = mongoose.model("TokenReportMail", tokenSchema);
