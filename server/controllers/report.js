var Report = require("../models/report");
var Machine = require("../models/machine");
var nodemailer = require("../nodemailer");
var TokenReportMail = require("../models/tokenReportMail");
var crypto = require("crypto");

function createReport(data, callback) {
  // TO DO check id machine
  // TO DO CHECK adress email
  if (!data.emailQuidam.match(/^.+@(vinci.be|student.vinci.be)$/)) {
    return callback(new Error(), null);
  }
  console.log(data.machineName)
  Machine.findOne({ 'name': data.machineName, 'active': true }, function (err, machine) {
    if (err) {
      return callback(err, null);
    }
    if (!machine) {
      console.log("machine non trouvée");
      return callback("Pas de machine trouvée", null);
    }
    console.log("machine ok");

    var report = new Report({
      emailQuidam: data.emailQuidam,
      machineName: data.machineName,
      description: data.description,
      priority: data.priority ? parseInt(data.priority, 10) : 2,
      picture: data.picture
    });
    report.save(function (err) {
      if (err) {
        console.log("erreur dans create Report - report");
        return callback(err, null);
      }

      var token = new TokenReportMail({
        _reportId: report._id,
        token: crypto.randomBytes(64).toString("hex")
      });

      token.save(function (err) {
        if (err) {
          console.log("erreur dans create report - token");
          return callback(err, null);
        }

        var mailOptions = {
          from: process.env.EMAIL_USERNAME || "no-reply@yourwebapplication.com",
          to: data.emailQuidam,
          subject: "Email de confirmation d'envoi de rapport !",
          text:
            "Bonjour,\n\n" +
            "Cliquez sur le lien suivant pour confirmer votre rapport: \nhttp://localhost:4200" +
            "/report/verify/" +
            token.token +
            "\n"
        };

        nodemailer.transporter.sendMail(mailOptions, (err, info) => {
          if (err) {
            return callback(err, null);
          }

          console.log("Message sent: %s", info.messageId);
          console.log(
            "Preview URL: %s",
            nodemailer.nodemailer.getTestMessageUrl(info)
          );
          console.log(report);
          return callback(null, report);
        });
      });
    });
  });
}

function verifyReport(data, callback) {
  TokenReportMail.findOne({ token: data.token }, function (err, token) {
    if (err) {
      // console.log("ici");
      return callback(err, null);
    }
    if (!token) {
      // console.log("là");
      return callback("Pas de token trouvé", null);
    }
    Report.findOne({ _id: token._reportId }, function (err, report) {
      if (err) {
        // console.log("plutot ici");
        return callback(err, null);
      }
      if (!report) {
        // console.log("non c'est ici");
        return callback("Pas de report avec cet id", null);
      }
      if (report.isVerified) {
        // console.log("finalement c'est là")
        return callback("Rapport déjà vérifié", null);
      }

      Report.findOneAndUpdate(
        { _id: report._id },
        { $set: { isVerified: true } },
        { new: true },
        function (err, report) {
          if (err) {
            // console.log("tu te trompes c'est ici");
            return callback(err, null);
          }
          return callback(null, report);
        }
      );
    });
  });
}

function submitReports(req, callback) {
  //, "isVerified": true
  Report.find({ 'state': "submit", "isVerified": true }, function (err, docs) {
    if (err) {
      return callback(err, null);
    }
    else {
      return callback(null, docs);
    }
  });
}

function validateReports(req, callback) {
  console.log("validateReports");
  Report.find({ 'state': { $ne: "submit" }, "isVerified": true }, function (err, docs) {
    if (err) {
      return callback(err, null);
    }
    else {
      return callback(null, docs);
    }
  });
}

function changeReport(report, callback) {
  var conditions = { _id: report._id };
  var update = report;

  Report.findOneAndUpdate(conditions, update, function (err) {
    if (err) {
      return callback(err);
    }
    else {
      return callback(null);
    }
  });
}

function removeReport(report, callback) {
  var conditions = { _id: report._id };

  //console.log(conditions);
  //console.log("coucou");
  //console.log(report);
  Report.findOneAndRemove(conditions, function (err) {
    if (err) {
      return callback(err);
    }
    else {
      return callback(null);
    }
  });
}

module.exports = {
  createReport,
  verifyReport,
  validateReports,
  submitReports,
  changeReport,
  removeReport
}

