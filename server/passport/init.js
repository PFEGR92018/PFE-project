var login = require("./login");
var User = require("../models/user");

module.exports = function(passport) {
    
  login(passport);

  // Passport needs to be able to serialize and deserialize users to support persistent login sessions
  passport.serializeUser(function(user, done) {
    // console.log('serializing user: ');
    // console.log(user);
    done(null, user._id);
  });

  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
